<?php
/**
 * Created by PhpStorm.
 * User: morontt
 * Date: 08.03.14
 * Time: 0:07
 */
namespace Battery\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Battery\BackendBundle\Entity\Repository\ConsumerRepository")
 * @DoctrineAssert\UniqueEntity("name")
 */
class Consumer
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(length=128, unique=true)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="LifeTime", mappedBy="consumer")
     */
    protected $timeItems;


    public function __construct()
    {
        $this->timeItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Consumer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add timeItems
     *
     * @param \Battery\BackendBundle\Entity\LifeTime $timeItems
     * @return Consumer
     */
    public function addTimeItem(\Battery\BackendBundle\Entity\LifeTime $timeItems)
    {
        $this->timeItems[] = $timeItems;

        return $this;
    }

    /**
     * Remove timeItems
     *
     * @param \Battery\BackendBundle\Entity\LifeTime $timeItems
     */
    public function removeTimeItem(\Battery\BackendBundle\Entity\LifeTime $timeItems)
    {
        $this->timeItems->removeElement($timeItems);
    }

    /**
     * Get timeItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTimeItems()
    {
        return $this->timeItems;
    }
}
