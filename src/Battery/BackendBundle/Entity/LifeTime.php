<?php
/**
 * Created by PhpStorm.
 * User: morontt
 * Date: 08.03.14
 * Time: 0:09
 */
namespace Battery\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Battery\BackendBundle\Entity\Repository\LifeTimeRepository")
 */
class LifeTime
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Battery
     *
     * @ORM\ManyToOne(targetEntity="Battery", inversedBy="timeItems")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    protected $battery;

    /**
     * @var Consumer
     *
     * @ORM\ManyToOne(targetEntity="Consumer", inversedBy="timeItems")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    protected $consumer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $dateEnd;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return LifeTime
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return LifeTime
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime 
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set battery
     *
     * @param \Battery\BackendBundle\Entity\Battery $battery
     * @return LifeTime
     */
    public function setBattery(\Battery\BackendBundle\Entity\Battery $battery)
    {
        $this->battery = $battery;

        return $this;
    }

    /**
     * Get battery
     *
     * @return \Battery\BackendBundle\Entity\Battery 
     */
    public function getBattery()
    {
        return $this->battery;
    }

    /**
     * Set consumer
     *
     * @param \Battery\BackendBundle\Entity\Consumer $consumer
     * @return LifeTime
     */
    public function setConsumer(\Battery\BackendBundle\Entity\Consumer $consumer)
    {
        $this->consumer = $consumer;

        return $this;
    }

    /**
     * Get consumer
     *
     * @return \Battery\BackendBundle\Entity\Consumer 
     */
    public function getConsumer()
    {
        return $this->consumer;
    }
}
