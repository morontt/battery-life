<?php

namespace Battery\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Battery\BackendBundle\Entity\Consumer;
use Battery\BackendBundle\Form\ConsumerType;

/**
 * Consumer controller.
 *
 * @Route("/consumer")
 */
class ConsumerController extends BaseController
{

    /**
     * Lists all Consumer entities.
     *
     * @Route("/", name="consumer")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BatteryBackendBundle:Consumer')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Consumer entity.
     *
     * @Route("/", name="consumer_create")
     * @Method("POST")
     * @Template("BatteryBackendBundle:Consumer:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Consumer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->flashSuccess('success');

            return $this->redirect($this->generateUrl('consumer_edit', array('id' => $entity->getId())));
        } else {
            $this->flashError('error');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Consumer entity.
     *
     * @Route("/new", name="consumer_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Consumer();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Consumer entity.
     *
     * @Route("/{id}/edit", name="consumer_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BatteryBackendBundle:Consumer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consumer entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Consumer entity.
     *
     * @Route("/{id}", name="consumer_update")
     * @Method("PUT")
     * @Template("BatteryBackendBundle:Consumer:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BatteryBackendBundle:Consumer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consumer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->flashSuccess('success');

            return $this->redirect($this->generateUrl('consumer_edit', array('id' => $id)));
        } else {
            $this->flashError('error');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Consumer entity.
     *
     * @Route("/{id}", name="consumer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BatteryBackendBundle:Consumer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Consumer entity.');
            }

            if ($entity->getTimeItems()->count() == 0) {
                $em->remove($entity);
                $em->flush();

                $this->flashSuccess('success');
            } else {
                $this->flashError('error');
            }
        } else {
            $this->flashError('error');
        }

        return $this->redirect($this->generateUrl('consumer'));
    }

    /**
     * Creates a form to delete a Consumer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consumer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn btn-danger',
                    ],
                ]
            )
            ->getForm();
    }

    /**
     * Creates a form to create a Consumer entity.
     *
     * @param Consumer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Consumer $entity)
    {
        $form = $this->createForm(new ConsumerType(), $entity, array(
            'action' => $this->generateUrl('consumer_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Creates a form to edit a Consumer entity.
     *
     * @param Consumer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Consumer $entity)
    {
        $form = $this->createForm(new ConsumerType(), $entity, array(
            'action' => $this->generateUrl('consumer_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
}
