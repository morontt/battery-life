<?php

namespace Battery\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Battery\BackendBundle\Entity\LifeTime;
use Battery\BackendBundle\Form\LifeTimeType;

/**
 * LifeTime controller.
 *
 * @Route("/lifetime")
 */
class LifeTimeController extends BaseController
{

    /**
     * Lists all LifeTime entities.
     *
     * @Route("/", name="lifetime")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BatteryBackendBundle:LifeTime')->getAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new LifeTime entity.
     *
     * @Route("/", name="lifetime_create")
     * @Method("POST")
     * @Template("BatteryBackendBundle:LifeTime:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new LifeTime();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->flashSuccess('success');

            return $this->redirect($this->generateUrl('lifetime_edit', array('id' => $entity->getId())));
        } else {
            $this->flashError('error');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new LifeTime entity.
     *
     * @Route("/new", name="lifetime_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new LifeTime();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing LifeTime entity.
     *
     * @Route("/{id}/edit", name="lifetime_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BatteryBackendBundle:LifeTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LifeTime entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing LifeTime entity.
     *
     * @Route("/{id}", name="lifetime_update")
     * @Method("PUT")
     * @Template("BatteryBackendBundle:LifeTime:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BatteryBackendBundle:LifeTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LifeTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->flashSuccess('success');

            return $this->redirect($this->generateUrl('lifetime_edit', array('id' => $id)));
        } else {
            $this->flashError('error');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a LifeTime entity.
     *
     * @Route("/{id}", name="lifetime_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BatteryBackendBundle:LifeTime')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LifeTime entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->flashSuccess('success');
        } else {
            $this->flashError('error');
        }

        return $this->redirect($this->generateUrl('lifetime'));
    }

    /**
     * Creates a form to delete a LifeTime entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lifetime_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn btn-danger',
                    ],
                ]
            )
            ->getForm();
    }

    /**
     * Creates a form to create a LifeTime entity.
     *
     * @param LifeTime $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(LifeTime $entity)
    {
        $form = $this->createForm(new LifeTimeType(), $entity, array(
            'action' => $this->generateUrl('lifetime_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Creates a form to edit a LifeTime entity.
     *
     * @param LifeTime $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(LifeTime $entity)
    {
        $form = $this->createForm(new LifeTimeType(), $entity, array(
            'action' => $this->generateUrl('lifetime_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
}
