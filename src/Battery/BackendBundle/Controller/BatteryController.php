<?php

namespace Battery\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Battery\BackendBundle\Entity\Battery;
use Battery\BackendBundle\Form\BatteryType;

/**
 * Battery controller.
 *
 * @Route("/battery")
 */
class BatteryController extends BaseController
{

    /**
     * Lists all Battery entities.
     *
     * @Route("/", name="battery")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BatteryBackendBundle:Battery')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Battery entity.
     *
     * @Route("/", name="battery_create")
     * @Method("POST")
     * @Template("BatteryBackendBundle:Battery:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Battery();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->flashSuccess('success');

            return $this->redirect($this->generateUrl('battery_edit', array('id' => $entity->getId())));
        } else {
            $this->flashError('error');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Battery entity.
     *
     * @Route("/new", name="battery_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Battery();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Battery entity.
     *
     * @Route("/{id}/edit", name="battery_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BatteryBackendBundle:Battery')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Battery entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Battery entity.
     *
     * @Route("/{id}", name="battery_update")
     * @Method("PUT")
     * @Template("BatteryBackendBundle:Battery:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BatteryBackendBundle:Battery')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Battery entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->flashSuccess('success');

            return $this->redirect($this->generateUrl('battery_edit', array('id' => $id)));
        } else {
            $this->flashError('error');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Battery entity.
     *
     * @Route("/{id}", name="battery_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BatteryBackendBundle:Battery')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Battery entity.');
            }

            if ($entity->getTimeItems()->count() == 0) {
                $em->remove($entity);
                $em->flush();

                $this->flashSuccess('success');
            } else {
                $this->flashError('error');
            }
        } else {
            $this->flashError('error');
        }

        return $this->redirect($this->generateUrl('battery'));
    }

    /**
     * Creates a form to delete a Battery entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('battery_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add(
                'submit',
                'submit',
                [
                    'label' => 'Delete',
                    'attr' => [
                        'class' => 'btn btn-danger',
                    ],
                ]
            )
            ->getForm();
    }

    /**
     * Creates a form to create a Battery entity.
     *
     * @param Battery $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Battery $entity)
    {
        $form = $this->createForm(new BatteryType(), $entity, array(
            'action' => $this->generateUrl('battery_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Creates a form to edit a Battery entity.
     *
     * @param Battery $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Battery $entity)
    {
        $form = $this->createForm(new BatteryType(), $entity, array(
            'action' => $this->generateUrl('battery_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
}
