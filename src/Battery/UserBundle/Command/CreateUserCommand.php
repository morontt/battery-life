<?php
/**
 * Created by PhpStorm.
 * User: morontt
 * Date: 28.03.14
 * Time: 22:44
 */
namespace Battery\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Battery\UserBundle\Entity\User;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('battery:user:create')
            ->setDescription('Create new user')
            ->addArgument('username', InputArgument::REQUIRED, 'username')
            ->addOption('password', 'p', InputOption::VALUE_OPTIONAL, 'password', 'admin');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getOption('password');

        $user = new User();
        $encoder = $this->getContainer()
            ->get('security.encoder_factory')
            ->getEncoder($user);

        $passwordHash = $encoder->encodePassword($password, $user->getSalt());
        $user->setUsername($username)
            ->setPassword($passwordHash);

        $errors = $this->getContainer()->get('validator')->validate($user);

        if (count($errors) > 0) {
            $output->writeln('');
            foreach ($errors as $error) {
                $output->writeln(sprintf('<error>Error: %s</error>', $error->getMessage()));
            }
            $output->writeln('');
        } else {
            $em = $this->getContainer()
                ->get('doctrine')
                ->getManager();

            $em->persist($user);
            $em->flush();

            $output->writeln('');
            $output->writeln(sprintf('<info>Create user: <comment>%s</comment></info>', $username));
            $output->writeln('');
        }
    }
}
