<?php
/**
 * Created by PhpStorm.
 * User: morontt
 * Date: 29.03.14
 * Time: 2:09
 */
namespace Battery\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('battery:user:update')
            ->setDescription('Update user by username')
            ->addArgument('username', InputArgument::REQUIRED, 'username')
            ->addOption('password', 'p', InputOption::VALUE_OPTIONAL, 'password', 'admin');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getOption('password');

        $em = $this->getContainer()
            ->get('doctrine')
            ->getManager();

        $user = $em->getRepository('BatteryUserBundle:User')
            ->findOneByUsername($username);

        if (!$user) {
            $output->writeln('');
            $output->writeln(sprintf('<error>Error: user "%s" not found</error>', $username));
            $output->writeln('');
        } else {
            $salt = md5(uniqid(null, true));

            $encoder = $this->getContainer()
                ->get('security.encoder_factory')
                ->getEncoder($user);

            $passwordHash = $encoder->encodePassword($password, $salt);
            $user->setSalt($salt)
                ->setPassword($passwordHash);

            $em->flush();

            $output->writeln('');
            $output->writeln(sprintf('<info>Update user: <comment>%s</comment></info>', $username));
            $output->writeln('');
        }
    }
}
